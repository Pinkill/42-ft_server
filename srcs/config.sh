# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    config.sh                                          :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: user42 <user42@student.42.fr>              +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2020/08/30 03:43:51 by user42            #+#    #+#              #
#    Updated: 2020/09/04 21:50:40 by user42           ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

#!/bin/bash

# Install PHPMyAdmin
wget -P srcs https://files.phpmyadmin.net/phpMyAdmin/5.0.2/phpMyAdmin-5.0.2-all-languages.tar.gz
tar -xvf srcs/phpMyAdmin-5.0.2-all-languages.tar.gz -C /srcs
mv srcs/phpMyAdmin-5.0.2-all-languages /var/www/phpmyadmin

# Install WordPress
wget -P srcs https://wordpress.org/wordpress-5.5.1.tar.gz
tar -xvf srcs/wordpress-5.5.1.tar.gz -C /var/www

# Configure Nginx and permissions
cp /srcs/default /etc/nginx/sites-available/default
rm /var/www/html/index.nginx-debian.html
cp /srcs/index.php /var/www/html/
cp /srcs/favicon.ico /var/www/
chown -R www-data:www-data /var/www

# Check for environment variable AUTOINDEX and if it is set off properly change nginx configuration file
if [ "$AUTOINDEX" = "off" ]
then sed -i "s|autoindex	on;|index	html/index.php;|g" /etc/nginx/sites-available/default
fi

# Start mysql service and create database
service mysql start
mysql -u root < srcs/db.sql

# Start server services
service php7.3-fpm start
service nginx start

# Output any errors from nginx server
cat /var/log/nginx/error.log

# Access terminal
/bin/bash
