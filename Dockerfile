# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    Dockerfile                                         :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: user42 <user42@student.42.fr>              +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2020/08/26 23:14:25 by user42            #+#    #+#              #
#    Updated: 2020/09/03 04:37:26 by user42           ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

#!/bin/bash
FROM debian:buster

LABEL maintainer="The Docker Executioner <mknezevi@student.42.fr>"

RUN echo "======> Setting up Debian Buster <======" \
&& apt-get -y update && apt-get -y upgrade \
&& echo "======> Installing PHP, NGINX, MYSQL and FastCG <======" \
&& apt-get install -y nginx mariadb-server php7.3 php7.3-fpm php7.3-mysql wget \
&& echo "======> Opening ports and copying sources to container <======";

EXPOSE 80
EXPOSE 443

COPY /srcs/* /srcs/

CMD bash /srcs/config.sh