# 42 ft_server

A project in the 42 study program. This project is about setting up a simple web server with docker. It uses Nginx web server and Wordpress, phpMyAdmin, MySQL services.

To see the documentation of the program and how it works, check "en.subject.pdf" file.

There is also a "Help" file which has some commands that will help with managing the project.

## Getting Started

The web server is pretty simple. It works with SSL protocol, which is included with the project.

The SQL database username is "admin" and password "password".

### Prerequisites

```
- docker
```

### Building the image

```
docker build --tag test:1.0
```

### Running the image

Check the name of the image:

```
docker images
```

Running the image:

```
docker run --rm -it -e AUTOINDEX=on -p 8080:80 -p 443:443 test
```

Good luck!